from application import app, celery
import csv
from io import StringIO
from datetime import date, timedelta
import requests
import pandas as pd
import matplotlib.pyplot as plt


def download_csv_data(index="covid_daily_data"):
    count=0
    today = date.today() - timedelta(days=1)
    today = today.strftime("%m-%d-%Y")
    response_text = requests.get("https://raw.githubusercontent.com/CSSEGISan"+\
        "dData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"\
            +today+".csv").text
    csv_data = StringIO(response_text)
    csv_dict =  csv.DictReader(csv_data,delimiter=',')
    data_list = []
    for row in csv_dict:
        data_list.append(dict(row))
    print('>>>',data_list)
    print(len(data_list))
    if app.elasticsearch.indices.exists(index):
        print('\n\n table exists')
        app.elasticsearch.indices.delete(index)
    app.elasticsearch.indices.create(index, body=app.config["MAPPING"])
    for row in data_list:
        count+=1
        print(str(count),row)
        
        app.elasticsearch.index(index=index, body=row)
    print('|||||||||||    covid data added     ||||||')

def get_covid_data():
    download_csv_data()
    json_covid19_data = app.elasticsearch.search(
        index='covid_daily_data', size=4000,
        body={"query": {'match_all': {}}}
    )
    return json_covid19_data

def filter_covid19_data():
    covid19_data = get_covid_data()
    print('data added')
    covid19_data = covid19_data["hits"]["hits"]
    covid19_data = [covid19_data[row]["_source"] for row in range(len(covid19_data))]
    covid19_data = pd.DataFrame(covid19_data)
    covid19_data["Confirmed"] = covid19_data["Confirmed"].astype(int)
    covid19_data["Deaths"] = covid19_data["Deaths"].astype(int)
    filtered_data = covid19_data.groupby(covid19_data["Country_Region"])

    return filtered_data


def get_covid19_cases_per_country(filtered_data):
    cases_per_country = filtered_data['Confirmed'].sum()
    cases_per_country = dict(cases_per_country)
    cases_per_country = list(zip(cases_per_country.keys(), cases_per_country.values()))
    cases_per_country.sort(key=lambda item: item[1], reverse=True)
    cases_per_country = cases_per_country[:10]
    countries = [item[0] for item in cases_per_country]
    cases = [item[1] for item in cases_per_country]
    # print(countries,  cases)

    return countries, cases


def plot_covid19_cases_per_countty(countries, cases):
    x_axis, y_axis = countries, cases
    plt.figure(figsize=(12, 10))
    plt.xticks(rotation=45)
    plt.bar(x_axis, y_axis)
    plt.title("Top 10 countries with covid-19 cases.")
    plt.xlabel("Country")
    plt.ylabel("Covid-19 cases")
    plt.savefig("application/static/covid19_cases.png")
    plt.clf()
    plt.cla()


def get_mortality_rate(filtered_data):
    deaths = filtered_data["Deaths"].sum()
    cases_per_country = filtered_data["Confirmed"].sum()
    result = {}

    for country in dict(deaths).keys():
        if cases_per_country[country] > 10000:
            result[country] = (deaths[country] / cases_per_country[country]) * 100
    
    result = list(zip(result.keys(), result.values()))
    result.sort(key=lambda item: item[1], reverse=True)
    result = result[:10]
    countries = [item[0] for item in result]
    mortality_rate = [item[1] for item in result]
    return countries, mortality_rate


def plot_graph_mortality_graph(countries, mortality_rate):
    plt.figure(figsize=(12, 10))
    plt.xticks(rotation=45)
    plt.bar(countries, mortality_rate)
    plt.title("Top 10 hightest mortality rate.")
    plt.xlabel("Country")
    plt.ylabel("Mortality Rate")
    plt.savefig("application/static/covid19_mortality_rate.png")
    plt.clf()
    plt.cla()


@celery.task(name="tasks.covid_daily_data")
def plot_graphs():
    filtered_data = filter_covid19_data()

    countries, cases = get_covid19_cases_per_country(filtered_data)
    print(countries, cases)
    plot_covid19_cases_per_countty(countries, cases)

    countries, mortality_rate = get_mortality_rate(filtered_data)
    plot_graph_mortality_graph(countries, mortality_rate)

