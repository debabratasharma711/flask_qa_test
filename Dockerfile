FROM mcr.microsoft.com/vscode/devcontainers/base:0-ubuntu-18.04

LABEL maintainer="Docker - flask app"

RUN apt-get update

RUN apt-get install -y python3 python3-pip

RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-amd64.deb

RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.6.2-amd64.deb.sha512

RUN shasum -a 512 -c elasticsearch-7.6.2-amd64.deb.sha512 

RUN sudo dpkg -i elasticsearch-7.6.2-amd64.deb

RUN sudo update-rc.d elasticsearch defaults 95 10

RUN wget http://download.redis.io/releases/redis-5.0.8.tar.gz

RUN tar xzf redis-5.0.8.tar.gz

RUN cd redis-5.0.8 make ; sudo make install

RUN pip3 install wheel

COPY . ./application

WORKDIR ./application

RUN pip3 install -r requirement.txt

EXPOSE 5000

ENTRYPOINT ["/bin/bash", "-c", "./docker_dep.sh"]


