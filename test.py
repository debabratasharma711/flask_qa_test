from application import app
import unittest
import json
from unittest.mock import patch


class FlaskTest(unittest.TestCase):

    def test_signup(self):
        tester = app.test_client(self)
        response = tester.post('/user',data=json.dumps(dict(username='saurabharya',email='saurabharya@gmail.com',\
            password='saurabharya123',confirm_password='saurabharya123')),headers={'Content-type': 'application/json'})
        self.assertEqual(response.status_code, 201)

    #--> Signup test cases
    
    def test_signup_fail(self):
        tester = app.test_client(self)

        response = tester.post('/user', data=json.dumps(dict(
            username='saurabh2', email='saurabh22@gmail.com',
            password='saurabh2', confirm_password='saurabh2')
            ), headers={'Content-type': 'application/json'})

        self.assertEqual(response.status_code, 409, msg=response.data)

    def test_signup_fail_wrong_pwd(self):
        tester = app.test_client(self)

        response = tester.post('/user', data=json.dumps(dict(
            username='yadav', email='yadav@gmail.com',
            password='yadav', confirm_password='yadav1')
            ), headers={'Content-type': 'application/json'})

        self.assertEqual(response.status_code, 400, msg=response.data)

    def test_signup_bad_method(self):
        tester = app.test_client(self)

        response = tester.put('/user', data=json.dumps(dict(
            username='saurabh2', email='saurabh22@gmail.com',
            password='saurabh2', confirm_password='saurabh2')
            ), headers={'Content-type': 'application/json'})

        self.assertEqual(response.status_code, 405, msg=response.data)


    #--> Login test cases
    def test_login_success(self):
        tester = app.test_client(self)

        response = tester.post('/login', data=json.dumps(dict(
            email='qwerty1@gmail.com', password='qwerty1')
            ), headers={'Content-type': 'application/json'})

        token = str(response.data, 'utf-8')
        self.assertEqual(response.status_code, 200, msg=response.data)

        return token

    def test_login_fail(self):
        tester = app.test_client(self)

        response = tester.post('/login', data=json.dumps(dict(
            email='qwerty1@gmail.com', password='qwerty11')
            ), headers={'Content-type': 'application/json'})

        self.assertEqual(response.status_code, 400, msg=response.data)

    def test_login_fail_bad_method(self):
        tester = app.test_client(self)

        response = tester.get('/login', data=json.dumps(dict(
            email='qwerty1@gmail.com', password='qwerty1')
            ), headers={'Content-type': 'application/json'})

        self.assertEqual(response.status_code, 405, msg=response.data)


    #--> Questions test cases

    #Get all questions test cases
    def test_get_all_questions(self):
        tester = app.test_client(self)

        response = tester.get('/questions')

        self.assertEqual(response.status_code, 200, msg=response.data)

    
    def test_get_question_404(self):
        tester = app.test_client(self)

        response = tester.get('/question')

        self.assertEqual(response.status_code, 404, msg=response.data)

    def test_get_all_questions_invalid_method(self):
        tester = app.test_client(self)

        response = tester.put('/questions')

        self.assertEqual(response.status_code, 405, msg=response.data)      
    #Get questions posted by a user
    def test_get_questions_by_uid(self):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.get('user/4/questions', headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data) 

    def test_get_questions_by_invalid_uid(self):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.get('user/100/questions', headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    def test_get_questions_by_uid_bad_method(self):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('user/100/questions', headers=headers)

        self.assertEqual(response.status_code, 405, msg=response.data)
    #Get specific question test cases
    def test_get_question_by_id(self):
        tester = app.test_client(self)

        response = tester.get('/questions/3')

        self.assertEqual(response.status_code, 200, msg=response.data)

    def test_get_question_by_invalid_id(self):
        tester = app.test_client(self)

        response = tester.get('/questions/10065223')

        self.assertEqual(response.status_code, 404, msg=response.data)
    
    def test_get_question_by_bad_id(self):
        tester = app.test_client(self)

        response = tester.get('/questions/sqr')

        self.assertEqual(response.status_code, 404, msg=response.data)

    def test_get_question_bad_method(self):
        tester = app.test_client(self)

        response = tester.post('/questions/30')

        self.assertEqual(response.status_code, 405, msg=response.data) 

    #Post question test cases
    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_post_question(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/questions', data=json.dumps(dict(
            title='ygsabjhasj who is the PM of SOUTH AFRICA?',
            description='description of test')), headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data) 

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_post_question_invalid_method(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/questions', data=json.dumps(dict(
            title='ygsabjhasj who is the PM of SOUTH AFRICA?',
            description='description of test')), headers=headers)
        self.assertEqual(response.status_code, 405, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_post_question_bad_rqst(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/questions', headers=headers)
        self.assertEqual(response.status_code, 400, msg=response.data)    
        
    
    #Update question test cases
    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_question(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('/questions/26', data=json.dumps(dict(
            title='How is docker image creatscsdcsded?!!!!',
            description='description of test!!!!!!')
            ), headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)   

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_question_unauthorised_request(self, mock_jwt_required):
        tester = app.test_client(self)
        headers = {'Content-Type': 'application/json'}
        response = tester.put('/questions/4', data=json.dumps(dict(
            title='How is docker image creatscsdcsded?!!!!',
            description='description of test!!!!!!')
            ), headers=headers)

        self.assertEqual(response.status_code, 401)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_question_invalid_id(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('/questions/501', data=json.dumps(dict(
            title='How is docker image creatscsdcsded?!!!!',
            description='description of test!!!!!!')
            ), headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_answer_invalid_id(self, mock_jwt_required):
        tester = app.test_client(self)
        headers = {'Content-Type': 'application/json'}
        response = tester.put('/answer/266', data=json.dumps(dict(
            answer_desc='jknsdaji iujkdaw  wjnm SOUTH AFRICAlllllllllll?')
            ), headers=headers)
        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_question_invalid_id_format(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('/questions/invalidid', data=json.dumps(dict(
            title='How is docker image creatscsdcsded?!!!!',
            description='description of test!!!!!!')
            ), headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_answer_invalid_id_format(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('/answer/saasc', data=json.dumps(dict(
            answer_desc='jknsdaji iujkdaw  wjnm SOUTH AFRICAlllllllllll?')
            ), headers=headers)
        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_question_invalid_request(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/questions/25', data=json.dumps(dict(
            title='How is docker image creatscsdcsded?!!!!',
            description='description of test!!!!!!')
            ), headers=headers)

        self.assertEqual(response.status_code, 405, msg=response.data)
    
    #Delete question test cases
    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_question(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/questions/79', headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)    

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_question_invalid_id(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/questions/501', headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_answer_invalid_id(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/answer/501', headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_question_invalid_id_format(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/questions/invalidid', headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_answer_invalid_id_format(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/answer/invalidid', headers=headers)

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_question_invalid_request(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/questions/25', headers=headers)

        self.assertEqual(response.status_code, 405, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_answer_invalid_request(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/answer/25', headers=headers)

        self.assertEqual(response.status_code, 405, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_question_unauthorised_request(self, mock_jwt_required):
        tester = app.test_client(self)
        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/questions/4', headers=headers)

        self.assertEqual(response.status_code, 401, msg=response.data)

    #--> Answers test cases
    def test_get_all_answers(self):
        tester = app.test_client(self)

        response = tester.get('/question/26/answer')

        self.assertEqual(response.status_code, 200, msg=response.data)

    def test_get_answer_by_invalid_id(self):
        tester = app.test_client(self)

        response = tester.get('/question/5000/answer')

        self.assertEqual(response.status_code, 404, msg=response.data)

    def test_get_answer_by_bad_id(self):
        tester = app.test_client(self)

        response = tester.get('/question/xyz/answer')

        self.assertEqual(response.status_code, 404, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_get_answer_by_userid(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.get('/user/4/answer', headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_modify_answer(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.put('/answer/5', data=json.dumps(dict(
            answer_desc='jknsdaji iujkdaw  wjnm SOUTH AFRICAlllllllllll?')
            ), headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_delete_answer(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.delete('/answer/10', headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)

    @patch('flask_jwt_extended.view_decorators.verify_jwt_in_request')
    def test_post_answer(self, mock_jwt_required):
        tester = app.test_client(self)

        headers = {'Content-Type': 'application/json'}
        response = tester.post('/question/26/answer', data=json.dumps(dict(
            answer_desc='jknsdaji iujkdaw  wjnm SOUTH AFRICA?'
            )), headers=headers)

        self.assertEqual(response.status_code, 200, msg=response.data)


if __name__ == '__main__':
    unittest.main()
