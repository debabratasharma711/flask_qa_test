sudo -i service elasticsearch start
nohup redis-server &
nohup celery -A application.celery beat --loglevel=info &
nohup celery -A application.celery worker --loglevel=info &
nohup python3 run.py

